package toptal.sample.restapi.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import toptal.sample.domain.TimeEntry;
import toptal.sample.repository.TimeEntryRepository;
import toptal.sample.restapi.ValidatingUserRepositoryDecorator;
import toptal.sample.restapi.dto.TimeEntryDto;
import toptal.sample.restapi.exception.DateParseException;
import toptal.sample.restapi.resource.ResourceCollection;
import toptal.sample.restapi.resource.TimeEntryResource;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.stream.Collectors;

import static java.util.Arrays.asList;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;
import static toptal.sample.restapi.controller.TimeEntryRestController.ADMIN;
import static toptal.sample.restapi.controller.TimeEntryRestController.OWNER;

@RestController
@RequestMapping("/api/users/{userName}/time-entries")
public class TimeEntryRestController {

    public static final String OWNER = "authentication.name == #userName";
    public static final String ADMIN = "hasRole('ADMIN')";

    private ValidatingUserRepositoryDecorator validatingUserRepositoryDecorator;

    private TimeEntryRepository timeEntryRepository;

    @PreAuthorize(ADMIN + " or " + OWNER)
    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<ResourceCollection<TimeEntryResource>> getTimeEntries(@PathVariable String userName,
                                                                                @RequestParam(required = false) Integer dateFrom,
                                                                                @RequestParam(required = false) Integer dateTo)  {

        validatingUserRepositoryDecorator.findAccountValidated(userName);

        Integer dateFromNotNull = dateFrom == null ? TimeEntryDto.DATE_MIN : dateFrom;
        Integer dateToNotNull = dateTo == null ? TimeEntryDto.DATE_MAX : dateTo;

        Collection<TimeEntryResource> timeEntryResourceCollection = timeEntryRepository
                .findByUserNameAndDateBetween(userName, dateFromNotNull - 1, dateToNotNull + 1)
                .stream().map((timeEntry -> new TimeEntryResource(timeEntry)))
                .collect(Collectors.toList());

        return ResponseEntity.ok(
                new ResourceCollection<>(timeEntryResourceCollection)
        );
    }


    @RequestMapping(method = RequestMethod.GET, path = "/{timeEntryId}")
    @PreAuthorize(ADMIN + " or " + OWNER)
    public ResponseEntity<TimeEntryResource> getTimeEntry(@PathVariable String userName,
                                                          @PathVariable String timeEntryId) {

        validatingUserRepositoryDecorator.findAccountValidated(userName);

        TimeEntry timeEntry = timeEntryRepository.findOne(timeEntryId);

        if (timeEntry == null) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }

        return ResponseEntity.ok(
                new TimeEntryResource(timeEntry)
        );
    }

    @RequestMapping(method = RequestMethod.POST)
    @PreAuthorize(ADMIN + " or " + OWNER)
    public ResponseEntity<TimeEntryResource> createTimeEntry(@PathVariable String userName,
                                                             @Validated @RequestBody TimeEntryDto timeEntryDto) {

        validatingUserRepositoryDecorator.findAccountValidated(userName);

        TimeEntry timeEntry = new TimeEntry();
        timeEntry.setUserName(userName);

        updateTimeEntry(timeEntry, timeEntryDto);

        TimeEntry savedTimeEntry = timeEntryRepository.save(timeEntry);

        return ResponseEntity.created(
                linkTo(methodOn(TimeEntryRestController.class).getTimeEntry(userName, savedTimeEntry.getId()))
                        .toUri()
        ).body(
                new TimeEntryResource(savedTimeEntry)
        );
    }

    @RequestMapping(method = RequestMethod.PUT, path = "/{timeEntryId}")
    @PreAuthorize(ADMIN + " or " + OWNER)
    public ResponseEntity<TimeEntryResource> modifyTimeEntry(@PathVariable String userName,
                                                             @PathVariable String timeEntryId,
                                                             @Validated @RequestBody TimeEntryDto timeEntryDto) {

        validatingUserRepositoryDecorator.findAccountValidated(userName);

        TimeEntry timeEntry = timeEntryRepository.findOne(timeEntryId);

        if (timeEntry == null) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }

        timeEntry.setUserName(userName);
        updateTimeEntry(timeEntry, timeEntryDto);

        TimeEntry savedTimeEntry = timeEntryRepository.save(timeEntry);

        return ResponseEntity.ok(
                new TimeEntryResource(savedTimeEntry)
        );
    }

    @RequestMapping(method = RequestMethod.DELETE, path = "/{timeEntryId}")
    @PreAuthorize(ADMIN + " or " + OWNER)
    public ResponseEntity<Void> deleteTimeEntry(@PathVariable String userName,
                                                @PathVariable String timeEntryId) {

        validatingUserRepositoryDecorator.findAccountValidated(userName);

        timeEntryRepository.delete(timeEntryId);

        return ResponseEntity.ok().build();
    }

    private void updateTimeEntry(TimeEntry timeEntry, TimeEntryDto timeEntryDto) {

        timeEntry.setDate(timeEntryDto.getDate());
        timeEntry.setDistance(timeEntryDto.getDistance());
        timeEntry.setTimeSeconds(timeEntryDto.getTimeSeconds());

    }

    @Autowired
    public void setValidatingUserRepositoryDecorator(ValidatingUserRepositoryDecorator validatingUserRepositoryDecorator) {
        this.validatingUserRepositoryDecorator = validatingUserRepositoryDecorator;
    }

    @Autowired
    public void setTimeEntryRepository(TimeEntryRepository timeEntryRepository) {
        this.timeEntryRepository = timeEntryRepository;
    }
}
